﻿$(document).ready(function () {
    $('#twitter_send').click(function () {
        var text = $('#twitter_text').val();
        $.ajax(
            {
                type: 'POST',
                url: '/Twitter/send',
                data: { text: text },
                dataType: 'html',
                success: function (reponse) {
                    $("#twitter-back").text("Message sent");
                    $("#twitter-back").css("background-color", "green");
                    $("#twitter_text").val("");
                    $("#twitter_send").css("border-radius", "15px 15px 0px 0px");   
                    console.log(reponse);                    
                },
                error: function (reponse) {
                    $("#twitter-back").text("Message error");
                    $("#twitter-back").css("background-color", "red");
                    $("#twitter_text").val("");
                    $("#twitter_send").css("border-radius", "15px 15px 0px 0px");
                    console.log(reponse);
                }
            }
        );
    });
    
    $('#mail_send').click(function () {
        var text = $('#mail_text').val();
        var dest = $('#mail_dest').val();
        $.ajax(
            {
                type: 'POST',
                url: '/Mail/send_mail',
                data: { text: text, dest: dest },
                dataType: 'html',
                success: function (reponse) {
                    $("#mail-back").text("Message sent");
                    $("#mail-back").css("background-color", "green");
                    $("#mail_text").val("");
                    $("#mail_send").css("border-radius", "15px 15px 0px 0px");
                    console.log(reponse);
                },
                error: function (reponse) {
                    $("#mail-back").text("Message error");
                    $("#mail-back").css("background-color", "red");
                    $("#mail_text").val("");
                    $("#mail_send").css("border-radius", "15px 15px 0px 0px");
                    console.log(reponse);
                }
            }
        );
    }); 
});