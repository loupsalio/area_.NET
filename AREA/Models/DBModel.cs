﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AREA.Models
{
    public class DBModel
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        public DBModel()
        {
            server = "localhost";
            database = "Area";
            uid = "root";
            password = "starwolf";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
           
                connection.Open();
                return true;
         
           
        }

 
        private bool CloseConnection()
        {
           
                connection.Close();
                return true;
          
        }
      
        public String getSecret(String email)
        {
            string query = "SELECT secret FROM twitter WHERE mail = '" + email + "'";
            String result = "";

            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                var tmp = dataReader.Read();
                if (tmp == true)
                result = dataReader.GetString(0);
                dataReader.Close();
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
            return result;
        }

        public String getCode(String email)
        {
            string query = "SELECT code FROM twitter WHERE mail = '" + email + "'";
            String result = null;

            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                var tmp = dataReader.Read();
                if (tmp == true)
                result = dataReader.GetString(0);
                dataReader.Close();
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
            return result;
        }

        public void new_user(String token, String token_secret, String mail)
        {
            string query = "INSERT INTO `twitter` (`id`, `mail`, `code`, `secret`) VALUES (NULL, '" + mail + "', '" + token +"', '" + token_secret +  "');";
      
            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);
            
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }

        }
        public String getfbauth(String email)
        {
            string query = "SELECT code FROM facebook WHERE mail = '" + email + "'";
            String result = null;

            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                var tmp = dataReader.Read();
                if (tmp == true)
                    result = dataReader.GetString(0);
                dataReader.Close();
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
            return result;
        }

        public void new_fbuser(String token, String mail)
        {
            string query = "INSERT INTO `facebook` (`id`, `mail`, `code`) VALUES (NULL, '" + mail + "', '" + token + "');";

            if (this.OpenConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, connection);

                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }

        }
    }
}