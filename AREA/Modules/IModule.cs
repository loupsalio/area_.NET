﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AREA.Modules
{
    interface IModule
    {
        int send(Object obj);

        int getStatus();

        Object getMsg();
    }

}
