﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tweetinvi.Models;

namespace AREA.Modules
{
    public class area_tweet
    {

        public String _msg;
        public IAuthenticatedUser _auth;

        public area_tweet(String msg, object auth)
        {
            _msg = msg;
            _auth = (IAuthenticatedUser)auth;
        }
    }
}