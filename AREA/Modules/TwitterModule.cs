﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tweetinvi;
using Tweetinvi.Models;

namespace AREA.Modules
{
    public class TwitterModule : IModule
    {
        public int send(object obj)
            {
            area_tweet tmp = (area_tweet)obj;
            tmp._auth.PublishTweet(tmp._msg);
            return 0;
        }

        public int getStatus()
        {
            return 0;
        }

        public Object getMsg()
        {
            return 0;
        }
    }
}