﻿using AREA.Models;
using AREA.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Tweetinvi;
using Tweetinvi.Models;

namespace AREA.Controllers
{
    public class HomeController : Controller
    {
        DBModel db = new DBModel(); 

        private List<IModule> getModules()
        {
            List < IModule > modules = new List<IModule>();
            modules.Add(new TwitterModule());
            return modules;
        }

        public ActionResult Index()
        {
            List<IModule> modules = getModules();
            if (Request.IsAuthenticated && Session["twitter"] == null)
            {
                String tmp = db.getCode(User.Identity.Name);
                String tmp_secret = db.getSecret(User.Identity.Name);
                if (tmp != null && tmp_secret != null)
                {
                    var userCreds = Auth.CreateCredentials("rnJyTZHaoe89xSUEuVYTN6udx", "EoBuQ7TA0IIZ41odSlVAHpa50a8DrX7h3RRetjbpK4cBkljRbr", tmp, tmp_secret);
                    Session["twitter"] = Tweetinvi.User.GetAuthenticatedUser(userCreds);
                }
            }
            if (Session["thread"] == null)
            {
                var tmp = new TwitterController();
                var name = User.Identity.Name;
                Session["thread"] = new Thread(() => tmp.get_followers(name));
            }
            if (Request.IsAuthenticated && Session["twitter"] != null)
            {
                var T = (Thread)Session["thread"];
                if (T.IsAlive == false)
                    T.Start();
            }

           /* if (Request.IsAuthenticated && Session["fbauth"] == null)
            {
                String tmp = db.getfbauth(User.Identity.Name);
                if (tmp != null)
                    Session["fbauth"] = tmp;
            }*/
                return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contacts";

            return View();
        }
    }
}