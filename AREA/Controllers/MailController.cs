﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace AREA.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Index()
        {
            return View();
        }

        [WebMethod]
        void send_mail(String msg, String dest)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress(User.Identity.Name);
            mail.To.Add(dest);
            mail.Subject = "Area Mail";
            mail.Body = msg;

            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential(User.Identity.Name, "password44");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }
    }
}