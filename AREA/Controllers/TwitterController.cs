﻿using AREA.Models;
using AREA.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Tweetinvi;
using Tweetinvi.Credentials.Models;
using Tweetinvi.Models;

namespace AREA.Controllers
{
    public class TwitterController : Controller
    {
        DBModel db = new DBModel();
        private TwitterModule _tmodule = new TwitterModule();
        private IAuthenticationContext _authenticationContext;


        public ActionResult TwitterAuth()
        {
            ConsumerCredentials appCreds = new ConsumerCredentials("rnJyTZHaoe89xSUEuVYTN6udx", "EoBuQ7TA0IIZ41odSlVAHpa50a8DrX7h3RRetjbpK4cBkljRbr");

            String redirectURL = "http://" + Request.Url.Authority + "/Twitter/ValidateTwitterAuth";
            _authenticationContext = AuthFlow.InitAuthentication(appCreds, redirectURL);

            Session["auth"] = _authenticationContext;

            return new RedirectResult(_authenticationContext.AuthorizationURL);
        }
        public ActionResult ValidateTwitterAuth()
        {
            DBModel db = new DBModel();
            var verifierCode = Request.Params.Get("oauth_verifier");
            if (verifierCode == null)
            {
                return new RedirectResult("/Home/index");
            }
            _authenticationContext = (IAuthenticationContext)Session["auth"];
            var userCreds = AuthFlow.CreateCredentialsFromVerifierCode(verifierCode, _authenticationContext);
            db.new_user(userCreds.AccessToken, userCreds.AccessTokenSecret, User.Identity.Name);
            Session["auth"] = null;

            Session["twitter"] = Tweetinvi.User.GetAuthenticatedUser(userCreds);
            ViewBag.User = Tweetinvi.User.GetAuthenticatedUser(userCreds);
            ViewBag.name = Tweetinvi.User.GetAuthenticatedUser(userCreds).Email;
            return new RedirectResult("/Home/Index");
        }

        [WebMethod]
        public String send(String text)
        {
            _tmodule.send(new area_tweet(text, Session["twitter"]));
            return "tweet : " + text;
        }

        public void get_followers(String name)
        {
            String tmp = db.getCode(name);
            String tmp_secret = db.getSecret(name);
            while (tmp == null || tmp_secret == null)
            { }
                var userCreds = Auth.CreateCredentials("rnJyTZHaoe89xSUEuVYTN6udx", "EoBuQ7TA0IIZ41odSlVAHpa50a8DrX7h3RRetjbpK4cBkljRbr", tmp, tmp_secret);
                var log = Tweetinvi.User.GetAuthenticatedUser(userCreds);

                int ll = log.FollowersCount; ;
            String mail = log.Email;

            while (true)
            {
                int actu = log.FollowersCount;
                String actu_mail = log.Email;
                if (ll < actu)
                {
                    log = Tweetinvi.User.GetAuthenticatedUser(userCreds);
                    FacebController sn = new FacebController();
                    sn.twitter_follower(1);
                    ll = actu;
                    log.PublishTweet("You got a new follower");
                }
                else if (ll > actu)
                {
                    FacebController sn = new FacebController();
                    sn.twitter_follower(2);
                    ll = actu;
                    log.PublishTweet("You lost a follower");
                }
                if (actu_mail != mail)
                {
                    FacebController sn = new FacebController();
                    sn.twitter_mail();
                    log.PublishTweet("You changed mail");
                }

                Thread.Sleep(5000);
            }
        }
    }
}
