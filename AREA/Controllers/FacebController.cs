﻿using Facebook;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace AREA.Controllers
{
    public class FacebController : Controller
    {
        int send_value = 0;

        string facebook_urlAuthorize_base = "https://graph.facebook.com/oauth/authorize";
        string facebook_urlGetToken_base = "https://graph.facebook.com/oauth/access_token";
        string facebook_AppID = "148017852500378";
        string facebook_AppSecret = "3e928177c3505848035e865a2beba4b5";
      
        public String post()
        {
            Session["msg_facebook"] = Request.Params["msg"];
            if (Request.Params["svalue"] == "Text")
                Session["svalue"] = 0;
            if (Request.Params["svalue"] == "Lien")
                Session["svalue"] = 1;
            if (Request.Params["svalue"] == "Image")
                Session["svalue"] = 2;
            Facebook_AuthorizeApplication();
            return (String)Session["msg_facebook"] + "sent";
        }

        String _tmp = "";

        public void twitter_follower(int i)
        {
            if (i == 1)
                _tmp = "New twitter follower !";
            if (i == 2)
                _tmp = "You lost twitter follower !";
            Facebook_AuthorizeApplication();
        }
        public void twitter_mail()
        {
                _tmp = "Your twitter mail have been updated!";
            Facebook_AuthorizeApplication();
        }
        public ActionResult send()
        {
            if (Request.Params["code"] != null)
            {
                string authorizationCode = (String)Request.Params["code"]; ;
                string access_token = Facebook_GetAccessToken(authorizationCode);
                if (access_token == "")
                {
                    ViewBag.txt = "fail";
                    Response.Write("Could not get access_token");
                    return View();
                }
                if (_tmp == "")
                    Facebook_WriteWall(access_token, (String)Session["msg_facebook"]);
                else
                {
                    Facebook_WriteWall(access_token, _tmp);
                }
                _tmp = "";
            }
            ViewBag.txt = "ok";
            return View();
        }

        private void Facebook_AuthorizeApplication()
        {

            string scope = "publish_actions";
            string urlAuthorize = facebook_urlAuthorize_base;
            urlAuthorize += "?client_id=" + facebook_AppID;
            urlAuthorize += "&redirect_uri=http://localhost:55639/faceb/send";
            urlAuthorize += "&scope=" + scope;

            Response.Redirect(urlAuthorize, true); 
        }
        FacebookClient fg = new FacebookClient();
        private string Facebook_GetAccessToken(string pAuthorizationCode)
        {
            string urlGetAccessToken = facebook_urlGetToken_base;
            urlGetAccessToken += "?client_id=" + facebook_AppID;
            urlGetAccessToken += "&client_secret=" + facebook_AppSecret;
            urlGetAccessToken += "&redirect_uri=http://localhost:55639/faceb/send";
            urlGetAccessToken += "&code=" + pAuthorizationCode;

            string responseData = RequestResponse(urlGetAccessToken);
            if (responseData == "")
            {
                return "";
            }
            var t = JObject.Parse(responseData);

            string access_token = t["access_token"].ToString();

            return access_token;
        }

        private void Facebook_WriteWall(string pAccessToken, string pMessage)
        {
            send_value = (int)Session["svalue"];
            string username = "me";
            string datatype = "feed";
            string urlWriteWall = "https://graph.facebook.com/" + username + "/" + datatype + "?access_token=" + pAccessToken;
            string entityMessage = "";

            if (send_value == 0)
            {
                entityMessage = "message=" + pMessage;
            }
            else if (send_value == 1)
            {
                entityMessage = "link=" + pMessage;
            }
            else if (send_value == 2)
            {
                entityMessage = "message=image";
                entityMessage = "&picture=" + pMessage;
            }


            HttpPost(urlWriteWall, entityMessage);
            }

        private string Facebook_GetRedirectUri()
        {
            string urlCurrentPage = Request.Url.AbsoluteUri.IndexOf('?') == -1 ? Request.Url.AbsoluteUri : Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf('?'));
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in Request.QueryString) { if (key != "code") { nvc.Add(key, Request.QueryString[key]); } }
            string qs = "home/index";
            foreach (string key in nvc)
            {
                qs += qs == "" ? "?" : "&";
                qs += key + "=" + nvc[key];
            }
            string redirect_uri = urlCurrentPage + qs; 

            return redirect_uri;
        }


        private string HttpPost(string pUrl, string pPostData)
        {
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(pUrl);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(pPostData);
            Stream requestWriter = webRequest.GetRequestStream(); 
            requestWriter.Write(bytes, 0, bytes.Length);
            requestWriter.Close();

            Stream responseStream = null;
            StreamReader responseReader = null;
            string responseData = "";
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                responseStream = webResponse.GetResponseStream();
                responseReader = new StreamReader(responseStream);
                responseData = responseReader.ReadToEnd();
            }
            catch (Exception exc)
            {
                throw new Exception("could not post : " + exc.Message);
            }
            finally
            {
                if (responseStream != null)
                {
                    responseStream.Close();
                    responseReader.Close();
                }
            }

            return responseData;
        }

        private string RequestResponse(string pUrl)
        {
            HttpWebRequest webRequest = System.Net.WebRequest.Create(pUrl) as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.ServicePoint.Expect100Continue = false;
            webRequest.Timeout = 20000;

            Stream responseStream = null;
            StreamReader responseReader = null;
            string responseData = "";
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                responseStream = webResponse.GetResponseStream();
                responseReader = new StreamReader(responseStream);
                responseData = responseReader.ReadToEnd();
            }
            catch (Exception exc)
            {
                Response.Write("<br /><br />ERROR : " + exc.Message);
            }
            finally
            {
                if (responseStream != null)
                {
                    responseStream.Close();
                    responseReader.Close();
                }
            }

            return responseData;
        }



   
    }
}